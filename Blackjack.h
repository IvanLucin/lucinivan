#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<random>
#include<algorithm>
#include<chrono>

using namespace std;

class cards
{
public:
	int number=0;
	char type=' ';
	vector<cards> deck;
	int ctr = 0;
	cards() {}
	cards(int,char);
	~cards() {}
	void setDeck();
	void printDeck() const;
	void shuffle();
	vector<cards> deal_card();
	cards get_card();

};

class player:public cards
{
	
public:
	string name;
	int br = 0;
	void set_name(string s) { this->name = s; }
	int total=0;
	vector<cards> hand;
	void printHand();
	void set_hand(vector<cards>);
	void total_points();
	void print_new_card();
	void clear_hand();

};
